# Web Indexing Project

This project focuses on web indexing, involving crawling, processing, and creating various indices for crawled documents.

## Getting Started

### Prerequisites

Make sure you have Python installed. You can install the required packages by running:

```bash
pip install -r requirements.txt
```

### Running Tests

To test different stemmatization methods, you can run the following command:

```bash
python3 test/test_stem.py
```

In the test/test_stem.py file, various NLTK stemmers are tested for their effectiveness in stemming French tokens. The selected stemmer is stemmer_snowball(), which, based on conducted tests, has proven to be more effective for French (the majority of tokens are in French). However, it's crucial to note that specifying the language before the stemming process is essential, and this aspect needs further development.

### Running the Main Workflow

To run the main workflow of crawling, processing, and indexing, use the following command:

```bash
python3 main.py
```

## Project Outputs

After running the main workflow, several JSON files are generated, each serving a specific purpose in capturing and organizing information extracted from crawled documents. Here's a brief explanation of each JSON file:

### `title.non_pos_index.json`

This file contains a non-positional index of titles extracted from crawled documents. Each token in the titles is associated with the count of occurrences.

### `mon_stemmer.title.non_pos_index.json`

Similar to the `title.non_pos_index.json`, this file is a non-positional index, but with the additional step of stemming. The Snowball Stemmer is applied to tokens in the titles, and each stemmed token is associated with its count, position, and the URL of the document.

### `title.pos_index.json`

This file represents a positional index of titles extracted from crawled documents. For each token, it includes a list of document positions where the token appears, along with the count of occurrences.

### `content.index.json`

This file captures an index of HTML content tokens extracted from crawled documents. It provides information on the positions of each token within the documents.

### `metadata.json`

This file contains statistical information about the crawled documents, including the total number of documents, total tokens, average tokens per document, and the top 10 tokens based on frequency.

These JSON files serve as valuable resources for analyzing and retrieving information from the crawled data, facilitating efficient querying and further exploration of the indexed content.

## Project Structure

- main.py: Main script to execute the entire workflow.
- test/test_stem.py: Unit test script for evaluating stemmers.
- statistics.py: Class for calculating statistics on crawled documents.
- text_processing_tools.py: Utility functions for text processing.
- requirements.txt: List of required Python packages.

## Indexers

- **Non-Positional Indexer** : non_pos_index.py

The NonPositionalIndexer class creates a non-positional index for tokens in crawled documents. It counts the occurrences of each token across documents.

- **Non-Positional Stemmed Indexer** : stem_index.py

The NonPosStemIndexer class creates a non-positional and stemmed index for tokens in crawled documents. It uses the Snowball Stemmer for stemming.

- **Positional Indexer** : pos_index.py

The PositionalIndexer class creates a positional index for tokens in crawled documents. It stores the positions of each token within the documents.

- **Content Indexer** : content_index.py

The ContentIndexer class creates an index for HTML content tokens in crawled documents. It stores the positions of each token within the documents.

## Statistics

The Statistic class calculates various statistics on the crawled documents, such as the total number of documents, global tokens, average tokens per document, token frequency distribution, and top 10 tokens.


## Collections Module

The `collections` module in Python provides alternatives to built-in types that can be more efficient and useful in certain scenarios. One of the classes within this module is `defaultdict`.


### defaultdict

In the context of this project, the use of `defaultdict` is particularly advantageous for handling token indices efficiently. When indexing tokens within documents, the `defaultdict` allows for the automatic initialization of data structures, such as lists, for each token encountered. This eliminates the need for explicit checks for key existence, providing a clean and concise way to manage default values for nonexistent keys.

#### Example:

```python
from collections import defaultdict

# Create a defaultdict with int as the default_factory
count_dict = defaultdict(int)

# Accessing a nonexistent key won't raise KeyError
count_dict['apple'] += 1
count_dict['banana'] += 1
count_dict['apple'] += 1

print(count_dict)
# Output: defaultdict(<class 'int'>, {'apple': 2, 'banana': 1})
```

## Author 

**LOUKILI IBTISSAM** 

