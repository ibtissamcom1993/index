import json
from collections import defaultdict
from text_processing_tools import extract_contents, tokenize_and_preprocess


class ContentIndexer:
    """
    This class is responsible for creating an index for HTML content tokens in a collection of crawled documents.
    It uses a defaultdict to store the positions and URLs of each token within the documents.

    Methods
    -------
    load_crawled_urls()
        Load crawled URLs from the JSON file.

    process_documents(crawled_data)
        Process the crawled documents, extract contents, tokenize and preprocess them, and update the index.

    save_index(filename='content.index.json')
        Save the content index to a JSON file.

    run()
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
    """
    def __init__(self):
        self.content_index = defaultdict(list)

    def load_crawled_urls(self):
        """
        Load crawled URLs from the JSON file.

        Returns
        -------
        list
            List of crawled documents.
        """
        with open('crawled_urls.json', 'r', encoding='utf-8') as file:
            crawled_data = json.load(file)
        return crawled_data

    def process_documents(self, crawled_data):
        """
        Process the crawled documents, extract contents, tokenize and preprocess them, and update the index.

        Parameters
        ----------
        crawled_data : list
            List of crawled documents.
        """
        contents = extract_contents(crawled_data)  
        for doc_id, (content, document) in enumerate(zip(contents, crawled_data)):
            tokens = tokenize_and_preprocess(content)
            for position, token in enumerate(tokens):
                self.content_index[token].append({'position': position, 'url': document['url']})

    def save_index(self, filename='content.index.json'):
        """
        Save the content index to a JSON file.

        Parameters
        ----------
        filename : str, optional
            The filename for saving the index, by default 'content.index.json'.
        """
        with open(filename, 'w', encoding='utf-8') as index_file:
            json.dump(self.content_index, index_file, ensure_ascii=False, indent=2)

    def run(self):
        """
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
        """
        crawled_data = self.load_crawled_urls()
        self.process_documents(crawled_data)
        self.save_index()
