from statistics import Statistic
from non_pos_index import NonPositionalIndexer
from stem_index import NonPosStemIndexer
from pos_index import PositionalIndexer
from content_index import ContentIndexer

def main():
    """
    Main function to run the entire workflow of crawling, processing, and indexing.

    This function initializes instances of various indexers and statistics calculator,
    runs each component of the workflow, and logs the operations.

    Returns
    -------
    None
    """ 
    # Initialize instances of indexers and statistics calculator
    statistics = Statistic()
    non_pos_indexer = NonPositionalIndexer()
    stem_indexer = NonPosStemIndexer()
    pos_indexer = PositionalIndexer()
    content_indexer = ContentIndexer()

    # Run each component of the workflow
    statistics.run()
    non_pos_indexer.run()
    stem_indexer.run()
    pos_indexer.run()
    content_indexer.run()

    
if __name__=="__main__":
    main()



