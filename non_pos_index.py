import json
from collections import defaultdict
from text_processing_tools import extract_titles, tokenize_and_preprocess

class NonPositionalIndexer:
    """
    This class is responsible for creating a non-positional index for token counts in a collection of crawled documents.
    It uses a defaultdict to store the count of each token.

    Methods
    -------
    load_crawled_urls()
        Load crawled URLs from the JSON file.

    process_documents(crawled_data)
        Process the crawled documents, extract titles, tokenize and preprocess them, and update the token counts.

    save_index(filename='title.non_pos_index.json')
        Save the non-positional index to a JSON file.

    run()
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
    """
    def __init__(self):
        self.non_pos_index = defaultdict(int)  # Use a simple defaultdict to store the count

    def load_crawled_urls(self):
        """
        Load crawled URLs from the JSON file.

        Returns
        -------
        list
            List of crawled documents.
        """
        with open('crawled_urls.json', 'r', encoding='utf-8') as file:
            crawled_data = json.load(file)
        return crawled_data

    def process_documents(self, crawled_data):
        """
        Process the crawled documents, extract titles, tokenize and preprocess them, and update the token counts.

        Parameters
        ----------
        crawled_data : list
            List of crawled documents.
        """
        titles = extract_titles(crawled_data)
        for title, _ in zip(titles, crawled_data):
            tokens = tokenize_and_preprocess(title)
            for token in tokens:
                self.non_pos_index[token] += 1  # Increment the count for each token

    def save_index(self, filename='title.non_pos_index.json'):
        """
        Save the non-positional index to a JSON file.

        Parameters
        ----------
        filename : str, optional
            The filename for saving the index, by default 'title.non_pos_index.json'.
        """
        with open(filename, 'w', encoding='utf-8') as index_file:
            json.dump(self.non_pos_index, index_file, ensure_ascii=False, indent=2)

    def run(self):
        """
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
        """
        crawled_data = self.load_crawled_urls()
        self.process_documents(crawled_data)
        self.save_index()
