import json
from collections import defaultdict
from text_processing_tools import extract_titles, tokenize_and_preprocess

class PositionalIndexer:
    """
    This class is responsible for creating a positional index for tokens in a collection of crawled documents.
    It uses a defaultdict to store the positions and count of each token within the documents.

    Methods
    -------
    load_crawled_urls()
        Load crawled URLs from the JSON file.

    process_documents(crawled_data)
        Process the crawled documents, extract titles, tokenize and preprocess them, and update the positional index.

    save_index(filename='title.pos_index.json')
        Save the positional index to a JSON file.

    run()
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
    """

    def __init__(self):
        self.pos_index = defaultdict(lambda: {"positions": [], "count": 0})

    def load_crawled_urls(self):
        """
        Load crawled URLs from the JSON file.

        Returns
        -------
        list
            List of crawled documents.
        """
        with open('crawled_urls.json', 'r', encoding='utf-8') as file:
            crawled_data = json.load(file)
        return crawled_data

    def process_documents(self, crawled_data):
        """
        Process the crawled documents, extract titles, tokenize and preprocess them, and update the positional index.

        Parameters
        ----------
        crawled_data : list
            List of crawled documents.
        """
        titles = extract_titles(crawled_data)
        for doc_id, (title, _) in enumerate(zip(titles, crawled_data)):
            tokens = tokenize_and_preprocess(title)
            for token in tokens:
                self.pos_index[token]["positions"].append(doc_id)
                self.pos_index[token]["count"]+=1

    def save_index(self, filename='title.pos_index.json'):
        """
        Save the positional index to a JSON file.

        Parameters
        ----------
        filename : str, optional
            The filename for saving the index, by default 'title.pos_index.json'.
        """
        with open(filename, 'w', encoding='utf-8') as index_file:
            json.dump(self.pos_index, index_file, ensure_ascii=False, indent=2)

    def run(self):
        """
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
        """
        crawled_data = self.load_crawled_urls()
        self.process_documents(crawled_data)
        self.save_index()
