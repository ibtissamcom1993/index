from bs4 import BeautifulSoup
import pandas as pd
import json
from collections import Counter
from urllib.parse import urlparse


class Statistic:
    """
    This class is responsible for calculating statistics on a collection of crawled documents.
    It includes functionalities to load crawled URLs, process documents, extract top tokens, calculate various statistics,
    and log crawler operations.

    Attributes
    ----------
    total_documents : int
        Total number of documents.
    total_tokens : int
        Total number of tokens across all documents.
    tokens_per_document : list
        List containing the number of tokens for each document.
    tracker : list
        List to track operations performed.
    """

    def __init__(self):
        # Initialize variables
        self.total_documents = 0
        self.total_tokens = 0
        self.tokens_per_document = []
        self.tracker = []

    def load_crawled_urls(self):
        """
        Load crawled URLs from the JSON file.

        Returns
        -------
        list
            List of crawled documents.
        """
        with open('crawled_urls.json', 'r', encoding='utf-8') as file:
            crawled_data = json.load(file)
            self.total_documents = len(crawled_data)
            return crawled_data

    def process_documents(self, crawled_data):
        """
        Process each document to count total tokens and tokens per document.

        Parameters
        ----------
        crawled_data : list
            List of crawled documents.
        """
        for document in crawled_data:
            content_tokens = document['content'].split() 
            self.total_tokens += len(content_tokens)
            self.tokens_per_document.append(len(content_tokens))

    def extract_top_token(self, freq, k):
        """
        Function that sorts a frequency dictionary and returns the top-k tokens.

        Parameters
        ----------
        freq : dict
            Frequency dictionary.
        k : int
            Number of top tokens to extract.

        Returns
        -------
        dict
            Top-k tokens and their frequencies.
        """
        sorted_freq = dict(sorted(freq.items(), key=lambda item: item[1], reverse=True))
        top_k = dict(Counter(sorted_freq).most_common(k))
        return top_k

    def calculate_statistics(self):
        """
        Calculate various statistics including the number of documents, global tokens, tokens per document,
        average tokens per document, token frequency distribution, maximum tokens in a document, and top 10 tokens.
        Stores information in metadata.json.
        """

        # Average of tokens by document
        average_tokens_per_document = self.total_tokens / self.total_documents

        # Token frequency distribution
        token_counts = Counter(self.tokens_per_document)

        # Maximum tokens in a document
        max_tokens_in_a_document = max(self.tokens_per_document)

        # Top 10 tokens
        top_10_tokens = self.extract_top_token(token_counts, 10)

        # Store information in metadata.json
        metadata = {
            'total_documents': self.total_documents,
            'total_tokens': self.total_tokens,
            'average_tokens_per_document': average_tokens_per_document,
            'top_10_tokens': top_10_tokens,
        }

        with open('metadata.json', 'w', encoding='utf-8') as metadata_file:
            json.dump(metadata, metadata_file, ensure_ascii=False, indent=2)

    def log_operation(self, operation):
        """
        Logs a crawler operation to the tracker.

        Parameters
        ----------
        operation : str
            The operation to log.
        """
        self.tracker.append(operation)
        print(f"Tracker: {operation}")

    def run(self):
        """
        Execute the entire workflow by loading crawled URLs, processing documents, and calculating statistics.
        """
        crawled_data = self.load_crawled_urls()
        self.log_operation(f"Start processing")
        self.process_documents(crawled_data)
        self.log_operation(f"Calculate statistics")
        self.calculate_statistics()




