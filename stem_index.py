import json
from collections import defaultdict
from nltk.stem import SnowballStemmer
from text_processing_tools import extract_titles, tokenize_and_preprocess, stem_text

## After testing different stemmers, we decided to continue with the Snowball Stemmer.

class NonPosStemIndexer:
    """
    This class is responsible for creating a non-positional and stemmed index for tokens in a collection of crawled documents.
    It uses a defaultdict to store the positions, count, and URLs of each stemmed token within the documents.

    Methods
    -------
    load_crawled_urls()
        Load crawled URLs from the JSON file.

    process_documents(crawled_data)
        Process the crawled documents, extract titles, tokenize and preprocess them, stem the tokens, and update the index.

    save_index(filename='mon_stemmer.title.non_pos_index.json')
        Save the non-positional and stemmed index to a JSON file.

    run()
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
    """
    def __init__(self, stemmer_language='french'):
        self.non_pos_stem_index = defaultdict(list)
        self.stemmer = SnowballStemmer(stemmer_language)

    def load_crawled_urls(self):
        """
        Load crawled URLs from the JSON file.

        Returns
        -------
        list
            List of crawled documents.
        """
        with open('crawled_urls.json', 'r', encoding='utf-8') as file:
            crawled_data = json.load(file)
        return crawled_data

    def process_documents(self, crawled_data):
        """
        Process the crawled documents, extract titles, tokenize and preprocess them, stem the tokens, and update the index.

        Parameters
        ----------
        crawled_data : list
            List of crawled documents.
        """
        titles = extract_titles(crawled_data)
        for doc_id, (title, document) in enumerate(zip(titles, crawled_data)):
            tokens = tokenize_and_preprocess(title)
            stemmed_tokens = stem_text(tokens, self.stemmer)
            for position, token in enumerate(stemmed_tokens):
                self.non_pos_stem_index[token].append({'position': position, 'count': 1, 'url': document['url']})

    def save_index(self, filename='mon_stemmer.title.non_pos_index.json'):
        """
        Save the non-positional and stemmed index to a JSON file.

        Parameters
        ----------
        filename : str, optional
            The filename for saving the index, by default 'mon_stemmer.title.non_pos_index.json'.
        """
        with open(filename, 'w', encoding='utf-8') as index_file:
            json.dump(self.non_pos_stem_index, index_file, ensure_ascii=False, indent=2)

    def run(self):
        """
        Execute the entire workflow by loading crawled URLs, processing documents, and saving the index.
        """
        crawled_data = self.load_crawled_urls()
        self.process_documents(crawled_data)
        self.save_index()


