import nltk
from nltk.stem import *
import unittest
from text_processing_tools import evaluate_stemmers


class TestIndex(unittest.TestCase):
    """
    This class defines a unit test for evaluating stemmers on a given phrase.

    Attributes
    ----------
    phrase : str
        The input phrase to be processed.
    tokens : list
        List of tokens obtained from the input phrase.

    Methods
    -------
    test_evaluate_stemmers()
        Unit test to evaluate stemmers on the provided phrase.
    """
 
    phrase = "Ce projet fait partie des projets d'indexation web"
    tokens = nltk.word_tokenize(phrase)
    evaluate_stemmers(tokens)