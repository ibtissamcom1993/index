import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import string
from nltk.stem import PorterStemmer, LancasterStemmer, SnowballStemmer

nltk.download('stopwords')
nltk.download('punkt')

def extract_titles(crawled_data):
    """
    Extract titles from a list of crawled documents.

    Parameters
    ----------
    crawled_data : list
        List of crawled documents.

    Returns
    -------
    list
        List of titles extracted from crawled documents.
    """
    return [document['title'] for document in crawled_data]

def extract_contents(crawled_data):
    """
    Extract content from a list of crawled documents.

    Parameters
    ----------
    crawled_data : list
        List of crawled documents.

    Returns
    -------
    list
        List of content extracted from crawled documents.
    """
    return[document['content'] for document in crawled_data]

def tokenize_and_preprocess(text):
        """
        Tokenize and preprocess the input text by converting to lowercase, removing punctuation, and stopwords.

        Parameters
        ----------
        text : str
            Input text.

        Returns
        -------
        list
            List of preprocessed tokens.
        """
        stop_words = set(stopwords.words('french'))

        # Tokenization
        tokens = word_tokenize(text.lower())

        # Remove punctuation and stopwords
        tokens = [token for token in tokens if token.isalnum() and token not in stop_words]

        return tokens

def stem_text(tokens, stemmer):
    """
    Apply stemming to a list of tokens using a specified stemmer.

    Parameters
    ----------
    tokens : list
        List of tokens.
    stemmer : nltk.stem.*
        Stemmer object from NLTK.

    Returns
    -------
    list
        List of stemmed tokens.
    """
    return [stemmer.stem(token) for token in tokens] 

def evaluate_stemmers(phrases):
    """
    Evaluate different stemmers on a list of phrases, printing the original and stemmed tokens.

    Parameters
    ----------
    phrases : list
        List of phrases for evaluation.
    """
    stemmers = {
        'PorterStemmer': PorterStemmer(),
        'LancasterStemmer': LancasterStemmer(),
        'SnowballStemmer': SnowballStemmer('french'),
    }

    for stemmer_name, stemmer in stemmers.items():
        print(f"Results for {stemmer_name}:")
        for phrase in phrases:
            tokens = word_tokenize(phrase.lower())
            stemmed_tokens = stem_text(tokens, stemmer)
            print(f"Original: {tokens}, Stemmed: {stemmed_tokens}")
        print('\n')



